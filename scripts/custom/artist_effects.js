(function($) {

	$(document).ready(function() {

    "use strict";
        
        const body = document.querySelector('body');
        
        window.setTimeout(() => {
            body.classList.add('loaded');
        }, 1000);

        /*================================= 
		ELEMENTS
        =================================*/
        
        /* const artistSection = document.querySelector('.artist_images_container');

        const aic = document.querySelector('.artist_images');

        const firstArtistImageSet = document.querySelector('.artist_images .artist_image_set:first-child'); */

        /*================================= 
		INITIAL LOAD
        =================================*/

        /* window.setTimeout(() => {
            firstArtistImageSet.classList.add('loading', 'active');
        }, 500);

        window.setTimeout(() => {
            firstArtistImageSet.classList.remove('loading');
        }, 1500); */
    
		/*================================= 
		ARTIST MOUSE MOVE EFFECT
		=================================*/

        /* // Cache coordinates of artist image container (aic = artistImageContainer)

        const aicCenterX = aic.offsetLeft + aic.offsetWidth / 2;

        const aicCenterY = aic.offsetTop + aic.offsetHeight / 2;

        let mousePositionX;
        let mousePositionY;

        let mousePositionXPercentage;
        let mousePositionYPercentage;

        function animateActiveImage() {

            // Save the loaded image set

            const artistImageLayers = document.querySelectorAll('.artist_image_set.active img.moveable');

            // Check each layer and move the coordinates appropriately.

            Array.from(artistImageLayers).forEach((artist) => {

                // Save this layers specific direction and speed.

                const direction = artist.dataset.direction;
                const speed = direction === 'with_cursor' ? artist.dataset.speed : - artist.dataset.speed;

                // Transform the layer

                artist.style.transform = `translate(${mousePositionXPercentage * speed}px ,${mousePositionYPercentage * speed}px)`;

            });

        }

        function mousemove(e) {

            // Get the mouse's x/y coordinates from the center of the image container.

            mousePositionX = e.pageX - aicCenterX;
            mousePositionY = e.pageY - aicCenterY;

            mousePositionXPercentage = mousePositionX / aicCenterX;
            mousePositionYPercentage = mousePositionY / aicCenterY;

            requestAnimationFrame(animateActiveImage);

        }

        artistSection.addEventListener('mousemove', mousemove); */

        /*================================= 
		Artist Name Hover Effect
		=================================*/

        /* // Elements

        const artistNames = document.querySelectorAll('.artist_name');

        // Show Image Set

        function showImageSet(e) {
            
            e.preventDefault();
                        
            // Save the artist name and corresponding image set

            const artist = this.dataset.artist;

            const activeName = document.querySelector('.artist_names .artist_name.active');

            const newImageSet = document.querySelector(`.artist_image_set.${artist}`);
            
            // Hide the currently active image set. Only if the artist name we are hovering to isn't the active one.

            const activeImageSet = document.querySelector('.artist_image_set.active');

            if(!activeImageSet.classList.contains(artist)) {

                // Activate the active class on the new artist name

                activeName.classList.remove('active');
                
                this.classList.add('active');

                // Remove the dynamically set transform property from the images in this set. So when/if they load in again they have a smooth transition.

                const activeImages = document.querySelectorAll('.artist_image_set.active img');

                Array.from(activeImages).forEach((image) => {
                    image.style.transform = '';
                });

                // Remove the active image set and show the new image set

                window.setTimeout(() => {
                    activeImageSet.classList.add('leaving');
                    activeImageSet.classList.remove('active');
                    newImageSet.classList.add('loading', 'active');
                }, 300);

                window.setTimeout(() => {
                    activeImageSet.classList.remove('leaving');
                    newImageSet.classList.remove('loading');
                }, 1300);

            }

        }

        // Hover to trigger showImageSet function

        Array.from(artistNames).forEach(artistName => {
            artistName.addEventListener('mouseenter', showImageSet);
            artistName.addEventListener('click', showImageSet);
            artistName.addEventListener('touchstart', showImageSet);
        }); */

    });
    
})(jQuery);