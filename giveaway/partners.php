<?php 
                   
            $partners = array(

                        'brilliant'=>array(
                            'url'=>'https://www.brilliant.co/',
                            'extension'=>'svg',
                            'width' => 109
                        ),

                        'deed'=>array(
                            'url'=>'http://godeed.today/',
                            'extension'=>'png',
                            'width' => 61
                        ),

                        'kirin' => array(
                            'url'=>'http://www.kirinichiban.com/',
                            'extension'=>'svg',
                            'width' => 144
                        ),

                        'matic'=>array(
                            'url'=>'https://maticnyc.com/',
                            'extension'=>'svg',
                            'width' => 125
                        ),

                        'medmen'=>array(
                            'url'=>'https://medmen.com/',
                            'extension'=>'svg',
                            'width' => 73
                        ), 

                        'trendland'=>array(
                            'url'=>'https://trendland.com/',
                            'extension'=>'png',
                            'width' => 125
                        ),

                        'tequila'=>array(
                            'url'=>'https://www.viva32.com/welcome',
                            'extension'=>'svg',
                            'width' => 54
                        ),
  
                );       
                           
                            
                    
                    
                    $length = count($partners);

                    $i = 0;

                    if($pair_last) {
                        if($length % 2 != 0) $i--;
                    }
                    
                    foreach($partners as $name=>$properties):
                        $i++; 
                        if($i % 2 == 0 || $length == $i) $is_pair = true;
                        else $is_pair = false;
                        
                ?>
                    <?=$is_pair ? '':'<div class="partners-pair-wrapper">'?>
                    <div class="partner">
                            <?= $properties['url'] ? '<a href="'.$properties['url'].'">':''?>
                                <img src="<?=$env['relative_path']?>/img/partners/giveaway/<?=$name?>_logo.<?=$properties['extension']?>" <?=$properties['width'] ? 'width="'.($properties['width']*$width_ratio).'"':''?> alt="<?=$name?>">
                            <?= $properties['url'] ? '</a>':''?>
                    </div>  
                    <?=$is_pair && $i != 0  ? '</div>':''?>   
                <?php
                    endforeach;
                ?>