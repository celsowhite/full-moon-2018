(function($) {

	$(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

		/*================================= 
		FITVIDS
		=================================*/

		/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

		/*=== Target div for fitVids ===*/

		$(".video_embed").fitVids();

	});

})(jQuery);