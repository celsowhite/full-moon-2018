<!doctype html>

<?php define(ROOT, realpath(dirname(__FILE__))); ?>
<?php define(SCRIPTS_VERSION, 4.0); ?>

<html class="no-js" lang="en">
    <head>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?= $env['title'] ? $env['title']. ' |' : '' ?>  Full Moon Music Festival</title>
<meta name="description" content="Full Moon is a boutique approach to a New York City music festival.">
<meta name="keywords" content="metronomy, james murphy, whitney, jackmaster, &ME, Adam Port, Rampa, Eclair Fifi, Ama Lou, Ge-Ology, Selvagem, Starchild & The New Romantic, nyc, governors island, new york city, matte, matte finish, matte projects, black, full moon, full moon fest, full moon festival moon festival nyc, festival nyc, max pollack, brett kincaid, matt rowean">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Facebook Meta -->

<meta property="og:title" content="<?= $env['title'] ? $env['title']. ' |' : '' ?> Full Moon Music Festival" />
<meta property="og:site_name" content="Full Moon Music Festival" />
<meta property="og:description" content="Full Moon is a boutique approach to a New York City music festival." />

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-49854214-8"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-49854214-8');
</script>

<!-- Google Code for Full Moon 2018 Conversion Page -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 813781781;
var google_conversion_label = "iUm2CO6krYEBEJWmhYQD";
var google_conversion_value = 1.00;
var google_conversion_currency = "USD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/813781781/?value=1.00&amp;currency_code=USD&amp;label=iUm2CO6krYEBEJWmhYQD&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<?php if($env['track']): ?>

       <!-- Facebook Pixel Code -->

       <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        // Insert Your Custom Audience Pixel ID below. 
        fbq('init', '200698297210581');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=200698297210581&ev=PageView&noscript=1"/></noscript>
        

<?php endif; ?>


<link rel='shortcut icon' href='<?=$env['relative_path']?>/favicon.ico' type='image/x-icon'/ >
        <link rel="stylesheet" href="<?=$env['relative_path']?>/fonts/font-awesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="https://cdn.plyr.io/3.2.0/plyr.css">
        <link rel="stylesheet" href="<?=$env['relative_path']?>/styles/plugin_styles.min.css?<?=SCRIPTS_VERSION?>">
        <link rel="stylesheet" href="<?=$env['relative_path']?>/styles/custom_styles.min.css?<?=SCRIPTS_VERSION?>">

</head>


<body class="<?=$env['layout']['body_class']?>">
                
        <div class="fm_background"></div>
        
        <!-- Mobile Navigation -->
        
        <div class="mobile_navigation">
            <div class="content_container">
                <ul class="mobile_navigation_list">
                    <li><a href="#artist_section">Home</a></li>
                    <li><a href="#gallery_section">Gallery</a></li>
                    <li><a href="#lineup_section">2018 Lineup</a></li>
                    <li><a href="#location_section">Location</a></li>
                    <li><a href="#partners_section">Partners</a></li>
                </ul>
            </div>            
        </div>

        <?php if(!$env['layout']['simple']): ?>
                <!-- Fixed Elements Container -->
        
                <section class="fixed_elements">
                    
                    <?php if($env['layout']['tickets_button']): ?>
                    
                        <!-- Main Navigation -->

                        <ul class="main_navigation">
                            <li><a href="#artist_section">Home</a></li>
                            <li><a href="#gallery_section">Gallery</a></li>
                            <li><a href="#lineup_section">2018 Lineup</a></li>
                            <li><a href="#location_section">Location</a></li>
                            <li><a href="#partners_section">Partners</a></li>
                        </ul>
                        
                        <div class="menu_icon"></div>
                        
                    <?php endif; ?>
            
                    <!-- Logo -->
                    
                    <div class="logo">
                        <a href="<?=$env['relative_path']?>"><img src="<?=$env['relative_path']?>/img/logo.png" /></a>
                    </div>
                                        
                    <?php if($env['layout']['tickets_button']): ?>
                        <!-- RSVP -->
                        <?php
                        /*
                        <div class="rsvp_button">
                            <a href="tickets" >
                                <img src="<?=$env['relative_path']?>/img/tickets_button.png" />
                            </a>
                        </div>
                        */
                        ?>
                    <?php endif; ?>
                
                                
                    <!-- A MATTE Project -->
                    
                    <p class="a_matte_project color_navy"><a href="https://matteprojects.com" target="_blank">A MATTE Project</a></p>
                    
                    <!-- Social Icons -->
                    
                    <ul class="social_icons">
                        <li><a href="https://www.facebook.com/fullmoonfest" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://www.instagram.com/fullmoonfest/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://twitter.com/fullmoonfestnyc" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="https://soundcloud.com/matteprojects" target="_blank"><i class="fab fa-soundcloud"></i></a></li>
                    </ul>
                                        
                </section>

        <?php endif; ?>

        <?php include($env['relative_path'].'/paint_splashes.php'); ?>

        

        <?php if($env['layout']['moon_gif']): ?>

                <!-- Moon Gif -->
                    
                <div class="moon_animation_container">
                    <img class="moon_animation desktop" src="<?=$env['relative_path']?>/img/moon_animation_desktop.gif" />
                    <img class="moon_animation mobile" src="<?=$env['relative_path']?>/img/moon_animation_mobile.gif" />
                </div>
                
                <!-- Type Layer -->
                
                <div class="type_layer_container">
                    <img class="type_layer desktop" src="<?=$env['relative_path']?>/img/type_layer_desktop.png" />
                    <img class="type_layer mobile" src="<?=$env['relative_path']?>/img/type_layer_mobile.png" />
                </div>

        <?php endif; ?>