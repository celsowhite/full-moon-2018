<ul class="artist_names">
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="metronomy">Metronomy</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="james-murphy">James Murphy<span class="super">(DJ Set)</span></span>
        <span class="after"></span>
    </li>
    <li>
        <span class="before"></span>
        <span class="artist_name" data-artist="whitney">Whitney</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="jackmaster">Jackmaster</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="adam-port">&ME, <span class="mobile_block">Adam Port</span></span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="sudan-archives">Sudan Archives</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="mira">Mira</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="eclair-fifi">Eclair Fifi</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="ama-lou">Ama Lou</span>
        <span class="after"></span>
    </li>
    
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="ge-ology">GE-OLOGY</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="selvagem">Selvagem</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="starchild"><span class="mobile_block">Starchild &</span> <span class="mobile_block">The New</span> Romantic</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="working-women">Working Women</span>
        <span class="after"></span>
    </li>
    <li>
    <span class="before"></span>
        <span class="artist_name" data-artist="donna-leak">Lime & Kotch</span>
        <span class="after"></span>
    </li>
</ul>