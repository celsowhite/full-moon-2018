(function ($) {

    $(document).ready(function () {

        "use strict";
        
        /*======================== 
		Signed Up From Ad
        ---
		Check for the presence of a parameter in the url to see if the user arrived at the site
        from an ad. If so then set the RSVP form input appropriately.
		========================*/
        
        // Elements
        
        const urlParams = new URLSearchParams(window.location.search);
        const rsvpFormAdInput = document.querySelector('.rsvp_form input[name="ad"]');
        
        // If the url has parameter utm_medium set as CPC then set the hidden form input value to true.
        
        if (urlParams.get('utm_medium') === 'CPC') {
            rsvpFormAdInput.value = 'true';
        };
        
		/*======================== 
		MAILCHIMP LIST SUBSCRIBE
        ---
		Use mailchimp API to subscribe users to the list.
		Different from regular mailchimp form submission in that the user doesn't have to opt in.
		========================*/

        // Set up variables

        let request;

        // Bind to the submit event of our form

        $(".rsvp_form").submit(function (e) {

            e.preventDefault();

            // Abort any pending request

            if (request) {
                request.abort();
            }

            const form = $(this);

            const rsvpMessage = form.closest('.rsvp_container').find('.rsvp_message');

            // Cache the form fields

            const inputs = form.find("input, select, button, textarea");

            // Serialize the data in the form

            const serializedData = form.serialize();

            // Disable the inputs for the duration of the Ajax request.

            inputs.prop("disabled", true);

            // Send the request to our php file that communicates with the Mailchimp API

            request = $.ajax({
                url: form.attr('action'),
                type: "post",
                data: serializedData
            });

            // Success Callback

            request.done(function (response, textStatus, jqXHR) {
                
                // Successful submission

                if (response === 'Success') {
                    rsvpMessage.html("<p>Success.</p>").fadeIn(300);
                    fbq('track', 'Lead', {
                        content_name: 'Full Moon RSVP',
                        content_category: 'RSVP'
                    });
                }

                // User is already subscribed to the list

                else if (response.indexOf('is already a list member') >= 0) {
                    rsvpMessage.html("<p>You have already RSVP'd.</p>").fadeIn(300).delay(3000).fadeOut(300);
                }

                // Other error. Unlikely there will be another type of error but just in case.

                else {
                    console.log('response:');
                    console.dir(response);
                    console.log('--------------------');
                    console.log('textStatus:');
                    console.dir(textStatus);
                    console.log('--------------------');
                    console.dir('jqXHR:');
                    console.dir(jqXHR);

                    rsvpMessage.html("<p>Error. Please try again.</p>").fadeIn(300).delay(3000).fadeOut(300);
                }

            });

            // Always Callback

            request.always(function () {

                // Reenable form inputs

                inputs.prop("disabled", false);

            });

        });

    });

})(jQuery);