"use strict";


function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

var removeSeparators = function(){

    var iTop = 0;
    var i = 0;
    var length = $(".artist_names li").length;
    

    $(".artist_names li").removeClass('no_separator').removeClass('no_separator_next').each(
        
        function() {
        
            var $el = $(this);
             
                var newTop = $el.find('.before').offset().top;
                if (  newTop > iTop || newTop == iTop ) {
                    $el.prev().addClass('no_separator');
                    $el.addClass('no_separator_next');
                } 
                
                if(i == (length-1)){
                    $el.addClass('no_separator');
                    if($(".artist_names").css('opacity') == 0){
                        $(".artist_names").animate({opacity:1},600);
                    }
                }

                iTop = $el.find('.after').offset().top;
                

            i++;
                
    });
    
   
};

var debouncedRmoveSeparators = debounce(removeSeparators,300);

window.addEventListener('resize', function(){
    debouncedRmoveSeparators();
});

$(function(){
    removeSeparators();
});