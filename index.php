
        
<?php 

$env = array(
            'track' => false,
            'relative_path' => '.',
            'layout' => array(
                'rsvp_modal' => true,
                'tickets_button' => true,
                'body_class' => 'home_page',
                'moon_gif' => true
            )
);

include($env['relative_path'].'/header.php'); ?>
        
        <!-- Home -->

        <section class="artist_section fm_section" id="artist_section">
            
            <?php // include($env['relative_path'].'/artists.php'); ?>
            
            <div class="artist_content_container">
                
                <iframe src="https://player.vimeo.com/video/280224336?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                
                <div class="thank_you_message">
                    <h2>Thank you for dancing with us at this year's Full Moon. The energy and love that you bring is what makes all of this come to life. <br>See you summer 2019!</h2>
                </div>
                
            </div>
            
        </section>
        
        <!-- Gallery -->
                
        <section class="fm_section padding_bottom gallery" id="gallery_section">

            <div class="gallery_grid">
            
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_10.jpg);"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_2.jpg);"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_6.jpg);"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_5.jpg);"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_3.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_7.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_8.jpg);"></div>

                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_9.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_1.jpg);"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_11.jpg);"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_12.jpg);"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_13.jpg); background-position: center 20%;"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_14.jpg); background-position: center 20%;"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_15.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_16.jpg);"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_17.jpg); background-position: center bottom;"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_18.jpg); background-position: center;"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_19.jpg); background-position: center 20%;"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_20.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_21.jpg);"></div>
                
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_22.jpg); background-position: center 20%;"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_23.jpg); background-position: center 20%;"></div>

                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_24.jpg); background-position: center top;"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_25.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_26.jpg);"></div>
                
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_27.jpg);"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_28.jpg);"></div>
                
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_29.jpg);"></div>
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_30.jpg);"></div>
                
                <div class="gallery_item half" style="background-image: url(img/gallery/gallery_33.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_32.jpg);"></div>
                <div class="gallery_item quarter" style="background-image: url(img/gallery/gallery_31.jpg);"></div>
                
            </div>

        </section>
                                
        <!-- Set Times -->
        
        <section class="fm_section padding_bottom lineup" id="lineup_section">

            <!-- Stages -->

            <div class="stages_container">
                <header class="stage_header">
                    <h1 class="text_navy">2018 Lineup</h1>
                    <div class="day_night">
                        <p class="text_navy">Day</br>2:00 pm - 10:00 pm</p>
                        <p class="text_navy">Night</br>10:00 pm - 4:00 am</p>
                    </div>
                </header>
                <div class="stages">
                    <div class="stage text_blue">
                        <h2>Solaar Stage</h2>
                        <ul class="stage_list">
                            <li>
                                <span class="setlist_time">7:30 - 10:00 pm</span>
                                Keinemusik
                            </li>
                            <li>
                                <span class="setlist_time">5:30 - 7:30 pm</span>
                                Mira
                            </li>
                            <li>
                                <span class="setlist_time">3:30 - 5:30 pm</span>
                                GE-OLOGY
                            </li>
                            <li>
                                <span class="setlist_time">2:00 - 3:30 pm</span>
                                Zephyr Ann b2b NSR
                            </li>
                        </ul>
                    </div>
                    <div class="stage text_red">
                        <h2>Full Moon Stage</h2>
                        <ul class="stage_list">
                            <li>
                                <span class="setlist_time">2:00 - 4:00 am</span>
                                Jackmaster
                            </li>
                            <li>
                                <span class="setlist_time">12:00 - 2:00 am</span>
                                James Murphy
                                <span class="super">(DJ Set)</span>
                            </li>
                            <li>
                                <span class="setlist_time">10:40 - 11:55 pm</span>
                                Metronomy
                            </li>
                            <li>
                                <span class="setlist_time">8:50 - 10:10 pm</span>
                                Eclair Fifi
                            </li>
                            <li>
                                <span class="setlist_time">7:30 - 8:25 pm</span>
                                Whitney
                            </li>
                            <li>
                                <span class="setlist_time">6:30 - 7:15 pm</span>
                                Sudan Archives
                            </li>
                            <li>
                                <span class="setlist_time">5:40 - 6:20 pm</span>
                                Starchild and the New Romantic
                            </li>
                            <li>
                                <span class="setlist_time">5:00 - 5:30 pm</span>
                                Ama Lou
                            </li>
                            <li>
                                <span class="setlist_time">3:00 - 4:50 pm</span>
                                Selvagem
                            </li>
                            <li>
                                <span class="setlist_time">2:00 - 3:00 pm</span>
                                Lloyd Evans
                            </li>
                        </ul>
                    </div>
                    <div class="stage text_green">
                        <h2>Jungle Ruins</h2>
                        <ul class="stage_list">
                            <li>
                                <span class="setlist_time">2:30 - 4:00 am</span>
                                Rechulski
                            </li>
                            <li>
                                <span class="setlist_time">1:30 - 2:30 am</span>
                                DJ Gui Machado
                            </li>
                            <li>
                                <span class="setlist_time">12:00 - 1:30 am</span>
                                WAi
                            </li>
                            <li>
                                <span class="setlist_time">10:00 - 12:00 am</span>
                                Working Women
                            </li>
                            <li>
                                <span class="setlist_time">9:00 - 10:00 pm</span>
                                Selvagem
                            </li>
                            <li>
                                <span class="setlist_time">3:00 - 9:00 pm</span>
                                Lime & Kotch
                            </li>
                        </ul>
                    </div>
                </div>
                                
            </div>
            
        </section>  
        
        <!-- Location -->
        
        <section class="location_section fm_section" id="location_section">
            
            <img src="img/location_map.png" class="location_map" />
            
            <div class="container">
                
                <video preload="auto" poster="img/video_poster.jpg" muted playsinline loop>
                    <source src="video/venue.mp4" type="video/mp4" />
                    <source src="video/venue.webm" type="video/webm" />
                </video>
                
                <div class="video_play_button"></div>
                
                <div class="location_info_container">
                    <div class="location_info_column">
                        <p>Located at the Knockdown Center just East of Williamsburg and just beyond the everyday, the Full Moon Festival's new location is an architectural marvel and inspiring space to express our aesthetic.</p>
                    </div>
                    <div class="location_info_column">
                        <p>A raw, open, and organic venue, the indoor and outdoor space holds 50,000 square feet of original brick, steel, and timber opening up to the night sky that watches over the city.</p>
                    </div>
                    <div class="location_info_column">
                        <div class="train_icon_container">
                            <div class="train_icon">L</div>
                            <p>to Jefferson Stop</p>
                        </div>
                    </div>
                </div>
            
            </div>
            
        </section>      
                       
        <!-- Partners -->
                                
        <div class="fm_section padding_bottom" id="partners_section">
            
            <div class="container">
            
                <img class="partners_title" src="img/partners_title.png" />
                
                <ul class="logo_grid">
                    <li><a href="https://www.aperol.com/" target="_blank"><img src="img/partners/festival/partner_logo_1.png" /></a></li>
                    <li><a href="https://dirtylemon.com/" target="_blank"><img src="img/partners/festival/partner_logo_4.png" /></a></li>
                    <li><a href="http://www.harmlessharvest.com/" target="_blank"><img src="img/partners/festival/partner_logo_3.png" /></a></li>
                    <li><a href="http://www.kirinichiban.com/" target="_blank"><img src="img/partners/festival/partner_logo_13.png" /></a></li>                    
                    <li><a href="http://kombrewcha.com/" target="_blank"><img src="img/partners/festival/partner_logo_6.png" /></a></li>
                    <li><a href="https://www.lyft.com/" target="_blank"><img src="img/partners/festival/partner_logo_15.png" /></a></li>
                    <li><a href="https://www.maisonmargiela.com/us" target="_blank"><img src="img/partners/festival/partner_logo_5.png" /></a></li>
                    <li><a href="http://oceanic.global/" target="_blank"><img src="img/partners/festival/partner_logo_16.png" /></a></li>
                    <li><a href="http://www.polarbev.com/" target="_blank"><img src="img/partners/festival/partner_logo_2.png" /></a></li>
                    <li><a href="https://www.surfrider.org/" target="_blank"><img src="img/partners/festival/partner_logo_14.png" /></a></li>
                    <li><a href="https://www.thealchemistskitchen.com/" target="_blank"><img src="img/partners/festival/partner_logo_17.png" /></a></li>
                    <li><a href="https://www.thewilliamsburghotel.com/" target="_blank"><img src="img/partners/festival/partner_logo_7.png" /></a></li>
                    <li><a href="https://www.viva32.com/" target="_blank"><img src="img/partners/festival/partner_logo_11.png" /></a></li>
                    <li><a href="https://www.instagram.com/wecycle.today/" target="_blank"><img src="img/partners/festival/partner_logo_18.png" /></a></li>
                </ul>
                
                <p class="partners_text">For partnership opportunities contact: <a href="mailto:info@matteprojects.com">info@matteprojects.com</a></p>
                
            </div>
            
            <?php include($env['relative_path'].'/paint_splashes_2.php'); ?> 
        
        </div>
                                                    
<?php include($env['relative_path'].'/footer.php'); ?>


