
        
<?php 

$env = array(
            'track' => true,
            'relative_path' => '..',
            'title' => 'Giveaway',
            'layout' => array(
                'rsvp_modal' => true,
                'tickets_button' => false,
                'body_class' => 'giveaway_page',
                'moon_gif' => true
            )
);


include($env['relative_path']."/header.php"); ?>
        
  
        <!-- Giveaway main -->

        <section class=" giveaway_container first" id="giveaway_info">


            <!-- Date (only only mobile & tablet)-->

            <div class="giveaway_date font_red">
                Knockdown Center<br>
                June, 30<sup>th</sup> &nbsp; 2018 
            </div>
            
            <div class="giveaway_info">
                    <h1 class="big-font giveaway_info_title">
                        Choose Fortune<br>
                        Giveaway Prizes
                    </h1>
                    <div class="h3 giveaway_info_detail">
                            Roundtrip Flights ($800 Voucher) to NYC by Kirin Ichiban<br>
                            Two VIP Passes to Full Moon<br>
                            $300 Gift Card for Medmen Products*<br>
                            One Hotel for Two Nights Stay in NYC<br>
                            One Brilliant Bicycle**<br>
                            Two Watches by Matic**<br>
                            $100 Uber Credit by VIVA Tequila
                    </div>
                    <div class="giveaway_info_detail_2 h3">
                        <span>
                            Over $2500 Worth of Prizes<br>
                            Winner will be Chosen June 22nd, 2018
                        </span>
                    </div>
                    <div class="giveaway_info_red font_red">
                        *Only Applicable at Medmen's CA Locations<br>
                        **Women's & Men's Styles Available
                    </div>


                    <a class="red_button modal_trigger" data-modal="rsvp_modal">
                        <img src="<?=$env['relative_path'] ?>/img/enter_here_button.png"  width="217" height="96" />
                    </a>
            </div>
            
        </section>





        <!-- Artists (only mobile & tablet) -->

        <section class="giveaway_container" id="giveaway_artists">

                <h3 class="big-font v3">Festival Lineup</h3>

                <div class="giveaway_artists_list">
                    <?php  include($env['relative_path'].'/artists.php'); ?>
                </div>
        
        </section>




        
        <!-- Partners -->

        <section class="giveaway_container" id="giveaway_partners">

                <div class="giveaway_partners_desktop">
                    <?php 
                        
                            $pair_last = true; 
                            $width_ratio = 1;
                            include($env['relative_path'].'/giveaway/partners.php');
                    ?>
                </div>

                <div class="giveaway_partners_mobile">
                    <?php 
                        
                            $pair_last = false; 
                            $width_ratio = 0.64;
                            include($env['relative_path'].'/giveaway/partners.php');
                    ?>
                </div>
        </section>





        <!-- Disclaimer -->
        <section class="giveaway_container last" id="giveaway_disclaimer">
                <small>
                This Promotions is only open to legal residents of the 50 United States and District of Columbia who are 21 years of age or older at the time of entry.  Promotion starts 6/14/18 and ends on 6/21/18.  Winner will be chosen at random on 6/23/18.  Winner will be notified via email.  Prizes are non-transfer.  No cash redemption or substitution will be allow.  Void where prohibited. This Promotion is in no way administered, executed or produced by Anheuser-Busch, LLC. 
                </small>
        </section>

        
        
       
    
<?php include($env['relative_path'].'/footer.php'); ?>
