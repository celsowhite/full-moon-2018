<?php 

$env = array(
            'track' => false,
            'relative_path' => '..',
            'title' => 'Tickets',
            'layout' => array(
                'simple' => true,
                'body_class' => 'tickets_page'
            )
);

include($env['relative_path'].'/header.php'); ?>

                        
        <!-- Tickets -->
        
        <div class="tickets_container fm_section container">
            
            <header class="tickets_header">
                <a class="logo" href="../">
                    <img src="../img/logo.png" />
                </a>
                <div class="tickets_header_info">
                    <h3 class="color_navy">Knockdown Center</h3>
                    <h3 class="color_navy">June, 30th 2018</h3>
                </div>
                <p class="color_navy">This is a 21+ event</p>
            </header>
            
            <div class="eventbrite_embed_container">
                
                <div id="eventbrite-widget-container-36398288260" class="eventbrite_iframe"></div>

                <script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>

                <script type="text/javascript">
                    window.EBWidgets.createWidget({
                        // Required
                        widgetType: 'checkout',
                        eventId: '36398288260',
                        iframeContainerId: 'eventbrite-widget-container-36398288260',

                        // Optional
                        iframeContainerHeight: 425
                    });
                </script>
                
            </div>
            
            <div class="eventbrite_fallback_container" style="display: none;">
                <p>It seems your browser version doesn't support Eventbrite's embedded checkout. Please click the below button to purchase tickets directly via Eventbrite.</p>
                <a href="https://www.eventbrite.com/e/full-moon-fest-2018-tickets-36398288260"><img src="../img/tickets_button.png" /></a>
            </div>

        </div>
        
<?php include($env['relative_path'].'/footer.php'); ?>
